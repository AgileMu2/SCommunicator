﻿using System.Collections.Generic;
using SCommunicator;

namespace Demo
{
    public class MyDataCollection : IAnalyzerCollection
    {
        private readonly MyData1 _data1 = new MyData1();
        private readonly MyData2 _data2 = new MyData2();
        private readonly IAnalyzer[] _innerArray;

        public MyDataCollection()
        {
            _innerArray = new IAnalyzer[] { _data1, _data2 };
        }

        public MyData1 Data1 { get { return _data1; } }
        public MyData2 Data2 { get { return _data2; } }

        public IAnalyzer this[int index]
        {
            get
            {
                return _innerArray[index];
            }
        }

        public IEnumerator<IAnalyzer> GetEnumerator()
        {
            return ((IEnumerable<IAnalyzer>) _innerArray).GetEnumerator();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return _innerArray.GetEnumerator();
        }
    }
}
