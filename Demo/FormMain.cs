﻿using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using SCommunicator;

namespace Demo
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            string[] ports = System.IO.Ports.SerialPort.GetPortNames();
            Array.Sort(ports);
            comboPort.Items.AddRange(ports);
            comboPort.SelectedIndex = comboPort.Items.Count > 0 ? 0 : -1;
            comboBaudrate.SelectedIndex = 0;
            device.OnRaw += device_OnRaw;

            //注册具体每个事件
            MyDataCollection results = device.Results as MyDataCollection;
            if (results != null)
            {
                results.Data1.GetNewData += Data1_GetNew;
                results.Data2.GetNewData += Data2_GetNew;
            }
        }

        void Data1_GetNew(AnalyzeResult<int> m)
        {
            ListViewItem item = new ListViewItem("MyData1");
            item.SubItems.Add(m.ToString());
            item.SubItems.Add(m.Valid ? "Get new" : "Data timeout");
            item.SubItems.Add(DateTime.Now.ToString("HH:mm:ss"));
            listViewData.Invoke((EventHandler)delegate 
            {
                listViewData.Items.Add(item);
                listViewData.EnsureVisible(listViewData.Items.Count - 1);
            });
        }

        void Data2_GetNew(AnalyzeResult<SampleData> m)
        {
            ListViewItem item = new ListViewItem("MyData2");
            item.SubItems.Add(m.ToString());
            item.SubItems.Add(m.Valid ? "Get new" : "Data timeout");
            item.SubItems.Add(DateTime.Now.ToString("HH:mm:ss"));
            listViewData.Invoke((EventHandler)delegate
            {
                listViewData.Items.Add(item);
                listViewData.EnsureVisible(listViewData.Items.Count - 1);
            });
        }

        void device_OnRaw(byte[] bytes)
        {
            StringBuilder builder = new StringBuilder();
            textBoxRaw.Invoke((EventHandler)delegate 
            {
                if (checkHex.Checked)
                {
                    foreach (byte b in bytes)
                    {
                        builder.Append(b.ToString("X2") + " ");
                    }
                }
                else
                {
                    builder.Append(device.Comm.Encoding.GetString(bytes));
                }
                textBoxRaw.AppendText(builder.ToString()); 
            });
        }

        SComm device = new SComm(new SerialPort(), new MyDataCollection());

        private void buttonConnect_Click(object sender, EventArgs e)
        {
            try
            {
                if (String.Compare(buttonConnect.Tag.ToString(), "OffLine", StringComparison.OrdinalIgnoreCase) == 0)
                {
                    SerialPortSetting sps = new SerialPortSetting();
                    sps.Baudrate = int.Parse(comboBaudrate.Text);
                    sps.Port = int.Parse(Regex.Match(comboPort.Text, @"\d+").Value);
                    if (device.Comm.Open(sps))
                    {
                        buttonConnect.Text = "Disconnect";
                        buttonConnect.Tag = "OnLine";
                    }
                }
                else
                {
                    device.Comm.Close();
                    buttonConnect.Tag = "OffLine";
                    buttonConnect.Text = "Connect";
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void checkWordWrap_CheckedChanged(object sender, EventArgs e)
        {
            textBoxRaw.WordWrap = checkWordWrap.Checked;
        }
    }
}
