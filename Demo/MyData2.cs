﻿using System;
using System.Globalization;
using SCommunicator;

namespace Demo
{
    /// <summary>
    /// 第二种解析出的数据模型
    /// </summary>
    public class MyData2 : BinaryAnalyzeResult<SampleData>
    {
        public MyData2()
        {
            this.Mask = new byte[] { 0xAA, 0xBB, 0xCC };
            this.TimeOut = 5;//超过5秒，收不到数据，则此数据无效。
            //自定义校验方法，演示为逐个相加和随便一个数字取模，我选择的是42
            this.Checksum = (buf, offset, count) =>
            {
                byte checksum = 0;
                for (int i = offset; i < offset + count; i++)
                {
                    checksum = (byte)((checksum + buf[i]) % 42);
                }
                return checksum;
            };
        }
        /// <summary>
        /// 数据解析协议
        /// 根据协议内容，对接收到的数据包进行解析，转换成Model抛出
        /// </summary>
        public override void Analyze()
        {
            int offset = Mask.Length + LenLength;//_mask.Length表示标记后的一个字节,_mask.Length+1表示标记后的第二个字节，有一个字节表示长度。
            this.Data.Version = BitConverter.ToInt32(Raw, offset + 0);
            this.Data.Voltage = BitConverter.ToSingle(Raw, offset + 4);
            this.Valid = true;//注意要设置数据有效状态
        }
    }
    /// <summary>
    /// 要转换成的Model
    /// </summary>
    public class SampleData
    {
        public int Version { get; set; }
        public float Voltage { get; set; }
        public SampleData()
        {
            Version = 0;
            Voltage = 0;
        }
        public override string ToString()
        {
            return string.Format("{0},{1}", Version.ToString(), Voltage.ToString(CultureInfo.InvariantCulture));
        }
    }
}
