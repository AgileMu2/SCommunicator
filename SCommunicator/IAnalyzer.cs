﻿using System.Collections.Generic;

namespace SCommunicator
{
    /// <summary>
    /// 数据解析接口
    /// </summary>
    public interface IAnalyzer
    {
        /// <summary>
        /// 分析数据
        /// </summary>
        void Analyze();
        /// <summary>
        /// 缓冲区数据查询
        /// </summary>
        /// <param name="buffer">数据缓冲区</param>
        /// <returns>查找结果</returns>
        SearchResult SearchBuffer(List<byte> buffer);

        /// <summary>
        /// 原始字节流
        /// </summary>
        byte[] Raw { get; set; }
    }
}
