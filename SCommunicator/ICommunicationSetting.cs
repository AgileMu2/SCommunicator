﻿namespace SCommunicator
{
    public interface ICommunicationSetting
    {
        /// <summary>
        /// 异步
        /// </summary>
        /// <returns></returns>
        byte[] AsBytes();
    }
}
