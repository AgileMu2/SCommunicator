﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace SCommunicator
{
    /// <summary>
    /// 数据解析抽象类
    /// </summary>
    /// <typeparam name="T">解析结果类</typeparam>
    public abstract class AnalyzeResult<T> : IAnalyzer where T : new()
    {
        private bool _valid;
        private byte[] _raw;
        private T _data;
        private int _passMiliSecond;
        private int _timeOut;
        private readonly Timer _timer;

        public delegate void GetNewDataHandler(AnalyzeResult<T> m);

        public event GetNewDataHandler GetNewData;

        public abstract void Analyze();

        /// <summary>
        /// 默认数据解包方法
        /// </summary>
        /// <param name="buffer">要分析的数据</param>
        /// <returns>分析结果</returns>
        public abstract SearchResult SearchBuffer(List<byte> buffer);

        protected AnalyzeResult()
        {
            TimerCallback callback = null;
            this._data = (default(T) == null) ? Activator.CreateInstance<T>() : default(T);
            this._timeOut = 0x7d0;
            callback = new TimerCallback(this.Callback);
            this._timer = new Timer(callback, null, -1, -1);
            this._data = (default(T) == null) ? Activator.CreateInstance<T>() : default(T);
        }

        
        private void Callback(object obj)
        {
            if (Valid)
            {
                _timer.Change(-1, -1);
                _data = (default(T) == null) ? Activator.CreateInstance<T>() : default(T);
                Valid = false;
            }
        }

        public static implicit operator T(AnalyzeResult<T> ar)
        {
            return ar.Data;
        }

        public override string ToString()
        {
            return this.Data.ToString();
        }

        /// <summary>
        /// 处理后的数据内容
        /// </summary>
        public T Data
        {
            get
            {
                return this._data;
            }
            set
            {
                this._data = value;
            }
        }

        public int PassMiliSecond
        {
            get
            {
                return this._passMiliSecond;
            }
            set
            {
                this._passMiliSecond = value;
            }
        }
        /// <summary>
        /// 未处理的数据
        /// </summary>
        public byte[] Raw
        {
            get
            {
                if (this._raw != null)
                {
                    return this._raw;
                }
                return new byte[0];
            }
            set
            {
                this._raw = value;
            }
        }
        /// <summary>
        /// 超时时间
        /// </summary>
        public int TimeOut
        {
            get
            {
                if (this._timeOut != -1)
                {
                    return (this._timeOut / 0x3e8);//0x3e8 = 1000
                }
                return this._timeOut;
            }
            set
            {
                this._timeOut = (value == -1) ? value : (value * 0x3e8);
            }
        }

        /// <summary>
        /// 验证数据
        /// </summary>
        public bool Valid
        {
            get
            {
                return this._valid;
            }
            set
            {
                this._valid = value;
                if (value)
                {
                    this._passMiliSecond = Environment.TickCount;
                    this._timer.Change(-1, -1);
                    this._timer.Change(this._timeOut, -1);
                }
                if (this.GetNewData != null)
                {
                    List<GetNewDataHandler> list = new List<GetNewDataHandler>();
                    Delegate[] invocationList = this.GetNewData.GetInvocationList();
                    int index = 0;
                    while (true)
                    {
                        if (index >= invocationList.Length)
                        {
                            break;
                        }
                        GetNewDataHandler item = (GetNewDataHandler)invocationList[index];
                        try
                        {
                            item((AnalyzeResult<T>)this);
                        }
                        catch (InvalidOperationException)
                        {
                            list.Add(item);
                        }
                        index++;
                    }
                    foreach (GetNewDataHandler handler in list)
                    {
                        this.GetNewData = (GetNewDataHandler)Delegate.Remove(this.GetNewData, handler);
                    }
                }
            }
        }
    }
}
