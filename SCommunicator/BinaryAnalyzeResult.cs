﻿using System;
using System.Collections.Generic;

namespace SCommunicator
{
    /// <summary>
    /// 二进制通讯协议解析类
    /// </summary>
    public abstract class BinaryAnalyzeResult<T> : AnalyzeResult<T> where T : new()
    {
        // 字段
        private byte[] _mask;
        protected CheckSumHandler Checksum;
        private int _stickLength;//定长数据-数据中不含数据长度的情况
        private int _lenLength;//数据长度值是几个字节的数据

        /// <summary>
        /// 构造函数 Protected
        /// </summary>
        protected BinaryAnalyzeResult()
        {
            _mask = new byte[0];
            Checksum = new CheckSumHandler(BinaryAnalyzeResult<T>.XorChecksum);
            _stickLength = 0;//置为0：默认非定长数据
            _lenLength = 1;
        }


        public override SearchResult SearchBuffer(List<byte> buffer)
        {
            SearchResult sResult = SearchResult.None;//结果
            int index = 0;		//数组游标
            int minLen = _mask.Length + 1;//一个数据包的最小长度 数据头+1
            while ((buffer.Count - index) > minLen)	//buff数据不够一个数据包就不循环
            {
                if (sResult != SearchResult.None)	//搜索结果不为数据头
                {
                    continue;	//直接执行下次循环
                }
                sResult = SearchResult.Mask;	// 满足以上条件之后--设置搜索结果为数据头
                int maskIndex = 0;
                while (maskIndex < _mask.Length)
                {
                    if (buffer[index + maskIndex] != _mask[maskIndex])
                    {
                        index += (maskIndex == 0) ? 1 : maskIndex;
                        sResult = SearchResult.None;
                        break;
                    }
                    maskIndex++;
                }
                if (sResult == SearchResult.Mask)
                {
                    //从数据包中读取数据长度，即在数据头后面的值，并且根据单字节还是多字节进行处理计算
                    int dataLength = 0;
                    if (_stickLength == 0)
                    {
                        //非定长情况
                        switch (_lenLength)
                        {
                            case 2:
                                dataLength = BitConverter.ToInt16(buffer.ToArray(), index + _mask.Length);//转换双字节数据
                                break;
                            case 4:
                                dataLength = BitConverter.ToInt32(buffer.ToArray(), index + _mask.Length);//转换4字节数据
                                break;
                            default:
                                dataLength = buffer[index + _mask.Length];//-转换单字节数据
                                break;
                        }
                    }
                    else
                    {
                        //定长情况
                        _lenLength = 0;//对于定长数据这个值应该为0，避免下面调用这个值出错
                        dataLength = _stickLength;
                    }

                    // 判断Buff中的数据是否够一个数据包
                    // 将右侧的_offset一道左侧计算
                    if (buffer.Count - index - (_mask.Length + _lenLength) - 1  < dataLength)//-1是减去校验位
                    {
                        return SearchResult.Mask;
                    }
                    //-数据校验
                    if ( (Checksum != null) && //--存在校验方法
                        (Checksum(buffer,(index + _mask.Length) + _lenLength, dataLength) //--数据长度
                         !=buffer[index + (_mask.Length + dataLength) + _lenLength]) )
                    {
                        buffer.RemoveRange(0,index);
                        return SearchResult.None;
                    }
                    else
                    {
                        //拷贝数据包到Raw，然后从buff中清除该数据
                        int count = ((_mask.Length + 1) + dataLength) + _lenLength;//数据包总长度
                        Raw = new byte[count];
                        buffer.CopyTo(index, Raw, 0, count);
                        buffer.RemoveRange(index, count);
                        return SearchResult.All;
                    }
                }
            }
            return SearchResult.None;
        }

        /// <summary>
        /// 异或校验方法
        /// </summary>
        public static byte XorChecksum(List<byte> buf, int index, int len)
        {
            byte num = 0;
            for (int i = index; i < (index + len); i++)
            {
                num ^= buf[i];
            }
            return num;
        }

        /// <summary>
        /// 定长数据长度-默认不含数据校验位
        /// </summary>
        public int StaticLength {
            get { return _stickLength; }
            set { _stickLength = value < 0 ? 0 : value; }
        }

        /// <summary>
        /// 数据长度值是几个字节的数据
        /// </summary>
        protected int LenLength
        {
            get { return this._lenLength; }
            set { this._lenLength = value; }
        }
        
        /// <summary>
        /// 数据头
        /// </summary>
        public byte[] Mask
        {
            get { return this._mask; }
            protected set { this._mask = value; }
        }
    }
}
